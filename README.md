## Description

Example with NestJS a complete TaskManager application, inspired by the Udemy course.

## References

- Class validatios - [https://github.com/typestack/class-validator](https://github.com/typestack/class-validator)

- TypeORM - [https://typeorm.io/#/](https://typeorm.io/#/)

- Course teacher's GitHub Repo - [https://github.com/arielweinberger/nestjs-course-task-management](https://github.com/arielweinberger/nestjs-course-task-management)

- Frontend Example - [https://github.com/arielweinberger/task-management-frontend](https://github.com/arielweinberger/task-management-frontend)

## Installation

Please follow the installation guide (Assuming you already have node and npm installed on your local machine)
- Installation guide - [https://docs.nestjs.com/](https://docs.nestjs.com/#installation)


```bash

$ npm i -g @nestjs/cli
$ nest new project-name

```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Stay in touch

- Author - [Fabio Bottan](mailto:fabio.bottan@action-ict.com)

## License

Nest is [MIT licensed](LICENSE).
