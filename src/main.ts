import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';
import * as config from 'config';

async function bootstrap() {
  const logger = new Logger('bootstrap');
  const app = await NestFactory.create(AppModule);

  //process.env.NODE_ENV Set this NODE Env variable if needed
  if(process.env.NODE_ENV === 'development'){
    //Only for developement and example purpose
    app.enableCors(); //Just enable CORS if you need to consume from any frontend using a differnt port
  }
  const configServer = config.get('server');

  const port = process.env.PORT || configServer.port;
  await app.listen(port);

  logger.log(`Application is listening on port ${port}`);
}
bootstrap();
