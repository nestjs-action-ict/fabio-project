import { User } from "src/auth/user.entity";
import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { TaskStatus } from "./task-status.enum";
@Entity()
export class Task extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    status: TaskStatus;

    //eager must be set true onlu for one side of relation
    @ManyToOne(type => User, user => user.tasks, { eager: false })
    user: User;

    //We want our entity to represent the shape of DB Table 
    //(actually its done under the hood by TypeORM) only to avoid any confusion
    @Column()
    userId: number;

}