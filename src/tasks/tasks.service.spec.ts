import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from 'src/auth/user.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTaskFilterDto } from './dto/get-task-filter.dto';
import { TaskStatus } from './task-status.enum';
import { TaskRepository } from './task.respository';
import { TasksService } from './tasks.service';

const mockTaskRepository = () => ({
  getTasks: jest.fn(),
  findOne: jest.fn(),
  createTask: jest.fn(),
  delete: jest.fn(),
});

const mockUser = { id: 1, username: 'Donald Duck', password: 'password', salt: 'salt'};

describe('TasksService', () => {
  //Avoid define class type to be more flexible with mocking
  let service;
  let taskRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TasksService,
        {provide: TaskRepository, useFactory: mockTaskRepository},
      ],
    }).compile();

    service = module.get<TasksService>(TasksService);
    taskRepository = module.get<TaskRepository>(TaskRepository);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getTasks', () => {
    it('get all task from repository', async () => {
      const filters: GetTaskFilterDto = {status: TaskStatus.DONE, search: 'some search string'};
      taskRepository.getTasks.mockResolvedValue('Some value');
      
      //call taskService.getTasks
      const result = await service.getTasks(filters, mockUser);
      expect(taskRepository.getTasks).toHaveBeenCalled();
      expect(result).toEqual('Some value');
    });
  });

  describe('getTasksById', () => {
    it('call repository.findOne() and successfully return the Task', async () => {
      const mockTask = {id: 1, title: 'Task 1', description: 'Description 1', status: TaskStatus.OPEN};
      taskRepository.findOne.mockResolvedValue(mockTask);
      
      //call taskService.getTaskById
      const result = await service.getTaskById(1, mockUser);
      expect(taskRepository.findOne).toHaveBeenCalledWith({where: {
        id: 1,
        userId: mockUser.id
      }} );
      expect(result).toEqual(mockTask);
    });

    it('Throw an error if not found', () => {
      const mockTask = null;
      taskRepository.findOne.mockResolvedValue(mockTask);
      
      expect(service.getTaskById(1, mockUser)).rejects.toThrow(NotFoundException);
    });
  });

  describe('createTask', () => {
    it('call taskRepository.createTask() and return new Task created', async () => {
      const mockTaskToCreate: CreateTaskDto = {title: 'New Task', description: 'some description'};
      const mockTaskCreated = {id: 1, title: 'Task 1', description: 'Description 1', status: TaskStatus.OPEN};
      taskRepository.createTask.mockResolvedValue(mockTaskCreated);
      
      //call taskService.createTask
      const result = await service.createTask(mockTaskToCreate, mockUser);
      expect(taskRepository.createTask).toHaveBeenCalled();
      expect(result).toEqual(mockTaskCreated);
    });
  });

  describe('deleteTask', () => {
    it('call taskRepository.deleteTask() and not Exception', async () => {
      taskRepository.delete.mockResolvedValue({affected: 1});
      
      const result = await service.deleteTaskById(1, mockUser);
      expect(taskRepository.delete).toHaveBeenCalledWith({
        id: 1,
        userId: mockUser.id
      });
    });

    it('call taskRepository.deleteTask() throw an error if task not deleted', () => {
      taskRepository.delete.mockResolvedValue({affected: 0});
      
      expect(service.deleteTaskById(1, mockUser)).rejects.toThrow(NotFoundException);
      expect(taskRepository.delete).toHaveBeenCalledWith({
        id: 1,
        userId: mockUser.id
      });
    });
  });

  describe('updateTaskStatus', () => {
    it('search task and update status and return new Task updated', async () => {
      const mockTask = {
        id: 1, 
        title: 'Task 1', 
        description: 'Description 1', 
        status: TaskStatus.OPEN,
        save: jest.fn().mockResolvedValue(true),
      };

      service.getTaskById = jest.fn().mockResolvedValue(mockTask);
      expect(service.getTaskById).not.toHaveBeenCalled();
      expect(mockTask.save).not.toHaveBeenCalled();
      //call taskService.updateTaskStatus
      const result = await service.updateTaskStatus(1, TaskStatus.DONE, mockUser);
      
      expect(service.getTaskById).toHaveBeenCalled();
      expect(mockTask.save).toHaveBeenCalled();
      expect(result.status).toEqual(TaskStatus.DONE);
    });
  });

});
